package com.balances.nsgpoc.utils;

public class NSGGlobalException extends RuntimeException {

    private String message;

    public NSGGlobalException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}