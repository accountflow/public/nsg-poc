package com.balances.nsgpoc.controller;

import com.balances.nsgpoc.manager.ReportManager;
import com.balances.nsgpoc.manager.ReportManager2;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import com.balances.nsgpoc.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xbrl._2003.instance.Xbrl;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

  @Autowired private ReportManager reportManager;
  @Autowired private Marshaller xbrliMarshaller;
  @Autowired  private ReportManager2 reportManager2;

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ReportManager.class);

  private static final Long[] allowedIds = {1L, 78L};

  @GetMapping(
      value = "/{companyId}",
      produces = Constants.XBRL_MEDIA_TYPE,
      consumes = MediaType.ALL_VALUE)
  public ResponseEntity<String> getTransactions(
      @PathVariable("companyId") Long companyId,
      @RequestParam(required = false) String startDate,
      @RequestParam(required = false) String endDate)
      throws JAXBException {

    if (!isIdAllowed(companyId)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Wrong company");
    }

    Optional<Date> start = Optional.empty();
    Optional<Date> finish = Optional.empty();
    SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
    try {
      if (StringUtils.isNoneBlank(startDate)) {
        start = Optional.of(formatDate.parse(startDate));
      }
    } catch (ParseException e) {
      LOGGER.error(e.getMessage());
    }
    try {
      if (StringUtils.isNoneBlank(endDate)) {
        finish = Optional.of(formatDate.parse(endDate));
      }
    } catch (ParseException e) {
      LOGGER.error(e.getMessage());
    }

    XbrliXbrl xbrl = reportManager.getReportForCompany(companyId, start, finish);

    Xbrl xbrl2 = reportManager2.getReportForCompany(companyId, start, finish);
    StringWriter writer = new StringWriter();

    xbrliMarshaller.marshal(xbrl2, writer);
    return new ResponseEntity<>(writer.toString(), HttpStatus.OK);
  }

  public static final ErrorHandler errorHandler =
      new ErrorHandler() {
        @Override
        public void warning(SAXParseException exception) throws SAXException {}

        @Override
        public void error(SAXParseException exception) throws SAXException {
          System.err.println(String.format("ERROR: %s", exception));
        }

        @Override
        public void fatalError(SAXParseException exception) throws SAXException {}
      };

  @GetMapping(
      value = "/{companyId}/{transactionId}",
      produces = Constants.XBRL_MEDIA_TYPE,
      consumes = MediaType.ALL_VALUE)
  public ResponseEntity<String> getTransactions(
      @PathVariable("companyId") Long companyId, @PathVariable("transactionId") String id)
      throws JAXBException {

    if (!isIdAllowed(companyId)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Wrong company");
    }

    var xbrl = reportManager.getReportForCompany(companyId, Optional.of(id));

    StringWriter writer = new StringWriter();

    xbrliMarshaller.marshal(xbrl, writer);

    return new ResponseEntity<>(writer.toString(), HttpStatus.OK);
  }

  /**
   * List allowed companyIds with transactions in the system
   *
   * @return allowed companyIds.
   * @throws JAXBException exception.
   */
  @GetMapping(produces = Constants.XBRL_MEDIA_TYPE, consumes = MediaType.ALL_VALUE)
  public ResponseEntity<Long[]> getCompanyIds() throws JAXBException {
    return new ResponseEntity<>(allowedIds, HttpStatus.OK);
  }

  private boolean isIdAllowed(Long companyId) {
    boolean result = false;
    for (Long id : allowedIds) {
      if (id.equals(companyId)) {
        result = true;
        break;
      }
    }
    return result;
  }
}
