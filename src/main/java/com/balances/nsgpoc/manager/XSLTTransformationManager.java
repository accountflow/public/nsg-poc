package com.balances.nsgpoc.manager;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class XSLTTransformationManager {

  public final String xsltPath = "xslt/report.xslt";

  private URL getResourceFileName(final String resource) {
    return getClass().getClassLoader().getResource(resource);
  }

  /**
   * Transform xml over xslt. Generate xbrl file from xml.
   *
   * @param is input
   * @param result stream result
   * @throws TransformerException
   */
  public void transformToXml(InputStream is, StreamResult result) throws TransformerException {
    var tFactory = TransformerFactory.newInstance();
    var transformer =
        tFactory.newTransformer(
            new StreamSource(new File(getResourceFileName(xsltPath).getFile())));
    transformer.transform(new StreamSource(is), result);
  }
}
