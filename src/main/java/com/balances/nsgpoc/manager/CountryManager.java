package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.model.Country;
import com.balances.nsgpoc.repository.СountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class CountryManager{

    @Autowired
    private СountryRepository countryRepository;

    /**
     * Get all countries
     * @return countries
     */
    public List<Country> getCountries() {
        return countryRepository.findAll();
    }
}