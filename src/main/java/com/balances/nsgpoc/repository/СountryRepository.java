package com.balances.nsgpoc.repository;

import com.balances.nsgpoc.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface СountryRepository extends JpaRepository<Country, Long> {}
