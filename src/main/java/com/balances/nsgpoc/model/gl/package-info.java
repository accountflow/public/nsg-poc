
@XmlSchema(
        xmlns = {
                @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
                @XmlNs(prefix="xbrll", namespaceURI="http://www.xbrl.org/2003/linkbase"),
                @XmlNs(prefix = "xbrli", namespaceURI = "http://www.xbrl.org/2003/instance"),
                @XmlNs(prefix = "xlink", namespaceURI ="http://www.w3.org/1999/xlink"),
                @XmlNs(prefix = "iso639", namespaceURI ="http://www.xbrl.org/2005/iso639"),
                @XmlNs(prefix = "iso4217", namespaceURI ="http://www.xbrl.org/2005/iso4217"),
                @XmlNs(prefix = "gl-cor", namespaceURI = "http://www.xbrl.org/int/gl/cor/2016-12-01"),
                @XmlNs(prefix = "gl-bus", namespaceURI = "http://www.xbrl.org/int/gl/bus/2016-12-01"),
                @XmlNs(prefix = "gl-muc", namespaceURI = "http://www.xbrl.org/int/gl/muc/2016-12-01")
        }
)
package com.balances.nsgpoc.model.gl;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;