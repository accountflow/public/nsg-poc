package com.balances.nsgpoc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "country")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country implements Serializable {
    private static final long serialVersionUID = -8944379478857278954L;

    @Column(name = "country_sid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long countrySID;

    @Column(name = "country_name")
    private String countryName;

    @Column(name = "country_code", unique = true)
    private String countryCode;

    @JoinColumn(name = "default_languagemaster_sid")
    @ManyToOne(fetch = FetchType.EAGER)
    private LanguageMaster languageMaster;
}
