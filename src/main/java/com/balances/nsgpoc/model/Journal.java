package com.balances.nsgpoc.model;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@Table(name = "JOURNAL")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {"journalID", "journalDescription", "journalType", "transaction"})
public class Journal implements Serializable {
  private static final long serialVersionUID = 7058942064043311153L;

  @Column(name = "journal_sid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @XmlTransient
  private Long journalSID;

  @XmlElement(name = "JournalID", required = true)
  private String journalID;

  @Column(name = "journal_description", length = 255)
  @XmlElement(name = "Description", required = true)
  private String journalDescription;

  @Column(name = "journal_type", length = 9)
  @XmlElement(name = "Type", required = true)
  private String journalType;

  @Cascade(org.hibernate.annotations.CascadeType.ALL)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "journal", fetch = FetchType.LAZY)
  @XmlElement(name = "Transaction")
  private Set<Transaction> transaction;
}
