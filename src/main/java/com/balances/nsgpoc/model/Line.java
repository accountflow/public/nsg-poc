package com.balances.nsgpoc.model;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
@Entity
@Table(
    name = "LINE",
    uniqueConstraints = {
      @UniqueConstraint(columnNames = {"record_id", "company_sid", "period_year"})
    })
public class Line implements Serializable {
  private static final long serialVersionUID = 5448661406208944155L;

  @Column(name = "line_sid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @XmlTransient
  private Long lineSID;

  @Column(name = "id")
  @XmlTransient
  private String id;

  @Column(name = "record_id", length = 28)
  @XmlElement(name = "RecordID", required = true)
  private String recordID;

  @Column(name = "account_id", length = 38)
  @XmlElement(name = "AccountID", required = true)
  private String accountID;

  @Column(name = "value_dates")
  @XmlElement(name = "ValueDate")
  @XmlSchemaType(name = "date")
  private Date valueDate;

  @Column(name = "source_document_id", length = 35)
  @XmlElement(name = "SourceDocumentID")
  private String sourceDocumentID;

  @XmlElement(name = "CustomerID")
  private String customerID;

  @XmlElement(name = "SupplierID")
  private String supplierID;

  @Column(name = "description", length = 255)
  @XmlElement(name = "Description", required = true)
  private String description;

  @JoinColumn(name = "debit_amount_sid")
  @OneToOne(cascade = CascadeType.ALL)
  @XmlElement(name = "DebitAmount")
  private Amount debitAmount;

  @JoinColumn(name = "credit_amount_sid")
  @OneToOne(cascade = CascadeType.ALL)
  @XmlElement(name = "CreditAmount")
  private Amount creditAmount;

  @Column(name = "reference_numbers", length = 35)
  @XmlElement(name = "ReferenceNumber")
  private String referenceNumber;

  @Column(name = "CID")
  @XmlElement(name = "CID")
  @XmlSchemaType(name = "nonNegativeInteger")
  private BigInteger cid;

  @Column(name = "due_dates")
  @XmlElement(name = "DueDate")
  @XmlSchemaType(name = "date")
  private Date dueDate;

  @XmlElement(name = "Quantity")
  private BigDecimal quantity;

  @XmlElement(name = "CrossReference")
  private String crossReference;

  @Column(name = "system_entry_times")
  @XmlElement(name = "SystemEntryTime")
  @XmlSchemaType(name = "date")
  private Date systemEntryTime;

  @XmlElement(name = "OwnerID")
  private String ownerID;

  @JoinColumn(name = "transaction_sid", nullable = false)
  @ManyToOne(fetch = FetchType.LAZY)
  @XmlTransient
  private Transaction transaction;

  @JoinColumn(name = "company_sid", nullable = false)
  @ManyToOne(fetch = FetchType.LAZY)
  @XmlTransient
  private Company company;

  @Column(name = "record_status", nullable = false)
  @XmlTransient
  private Boolean recordStatus;

  @Column(name = "createdDate", nullable = false)
  @XmlTransient
  private Date createdDate;

  @Column(name = "createdBy", length = 70)
  @XmlTransient
  private String createdBy;

  @Column(name = "modifiedDate")
  @XmlTransient
  private Date modifiedDate;

  @Column(name = "modifiedBy", length = 70)
  @XmlTransient
  private String modifiedBy;

  @Column(name = "modified_count", nullable = false)
  @XmlTransient
  private Integer modifiedCount;

  @Column(name = "period")
  @XmlTransient
  private Integer period;

  @Column(name = "period_year")
  @XmlTransient
  private Integer periodYear;

  @Column(name = "ientity_code")
  @XmlTransient
  private String iEntityCode;

  @Column(name = "general_ledger_account_sid")
  @XmlTransient
  private String generalLedgerAccountSID;

  @Column(name = "comment", length = 250)
  @XmlTransient
  private String comment;

  @Column(name = "is_partial_line")
  @XmlTransient
  private Boolean isPartialLine;

  @Column(name = "is_partially_completed")
  @XmlTransient
  private Boolean isPartiallyCompleted;

  @Column(name = "is_non_reconcilable")
  @XmlTransient
  private Boolean isNonReconcilable;

  @Column(name = "user_comment", length = 250)
  @XmlTransient
  private String userComment;

  @Column(name = "term_type")
  @XmlTransient
  private String termType;
}
