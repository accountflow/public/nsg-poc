package com.balances.nsgpoc.model.gl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestMapping {

  @org.junit.jupiter.api.Test
  public void testXbrl() {

    ObjectFactory of = new ObjectFactory();

    XbrliXbrl xbrl = of.createXbrliXbrl();

    XbrliXbrl.GlCorAccountingEntries ae = new XbrliXbrl.GlCorAccountingEntries();

    XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo info =
        of.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfo();
    XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo.GlCorEntriesType t =
        of.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorEntriesType();
    t.setContextRef("journal_context");
    t.setDataValue("journal");
    info.setGlCorEntriesType(t);

    xbrl.setGlCorAccountingEntries(of.createXbrliXbrlGlCorAccountingEntries());
    xbrl.getGlCorAccountingEntries().setGlCorDocumentInfo(info);

    xbrl.setGlCorAccountingEntries(ae);

    ae.setGlCorDocumentInfo(info);

    String content = jaxbObjectToXML(xbrl);
    assertTrue(
        content.contains(
            "<gl-cor:entriesType contextRef=\"journal_context\">journal</gl-cor:entriesType>"));
  }

  private static String jaxbObjectToXML(XbrliXbrl country) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(XbrliXbrl.class);

      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

      StringWriter sw = new StringWriter();

      jaxbMarshaller.marshal(country, sw);

      String content = sw.toString();
      return content;
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return "";
  }
}
